const express = require('express');
let router = express.Router();
const Controller = require('../controllers/product.controller')
const controller = new Controller()
router.get('/:category', controller.getProductInCategory);

module.exports = router;