const express = require('express');
let router = express.Router();
const Controller = require('../controllers/category.controller')
const controller = new Controller()
router.get('/', controller.getAll);

module.exports = router;