
class CategoryDAO {
    constructor() {
        this.db = require('../libs/db')
    }
    async getAll() {
        try {
            const data = await this.db.query('SELECT * FROM category')
            return data;
        } catch (ex) {
            console.log(ex);
        }
        return []
    }
}

module.exports = CategoryDAO;