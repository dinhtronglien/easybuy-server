const CategoryDAO = require('../DAO/category.DAO')
class Category {
    constructor() {
        this.categoryDAO = new CategoryDAO();
    }

    async getAll() {
        const data = await this.categoryDAO.getAll();
        return data;
    }
}

module.exports = Category;