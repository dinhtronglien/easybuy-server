const CategoryService = require('../services/category.service')
const categoryService = new CategoryService();
const Base = require("./base");

class CategoryController extends Base {
    constructor() {
        super()
    }

    getAll = async (req, res, next) => {
        const data = await categoryService.getAll();
        return res.status(this.StatusCodes.OK).json({ data })
    }
}

module.exports = CategoryController;