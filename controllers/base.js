const HttpStatusCodes = require('http-status-codes')
const ErrorMessage = require('../constants/ErrorMessage')
const ErrorCode = require('../constants/ErrorCode')

class Base {
    // StatusCodes = HttpStatusCodes
    constructor() {
        this.StatusCodes = HttpStatusCodes;
    }

}

module.exports = Base;