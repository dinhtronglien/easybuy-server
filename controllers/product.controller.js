const Base = require("./base");
const ErrorMessage = require('../constants/ErrorMessage')
const ErrorCode = require('../constants/ErrorCode');
const { clientError ,clientException} = require("../utils/httpResponse");

class CategoryController extends Base {
    constructor() {
        super()
    }

    getProductInCategory = (req, res) => {
        try {
            let { params: { category } } = req;
            category = parseInt(category);
            isNaN(category) && (category = 0)
            
            if (category <= 0)
                return clientError(res, ErrorCode.INPUT_IS_INVALID)

        } catch (ex) {

        }
        return clientException(res)
    }
}

module.exports = CategoryController;