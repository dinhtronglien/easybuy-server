const { ErrorCode } = require("./ErrorCode");

class ErrorMessage {
    constructor(code, message = '', data = {}) {
        this.code = code || ErrorCode.EXCEPTION;
        this.message = message || '';
        this.data = data || {};
    }

    response() {
        return { code: this.code, message: this.message, data: this.data }
    }
}

module.exports = ErrorMessage;