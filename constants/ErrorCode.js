const ErrorCode = {
    INPUT_IS_INVALID: -100,
    EXCEPTION: -200
}

module.exports = ErrorCode