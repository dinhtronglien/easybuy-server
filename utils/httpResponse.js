const ErrorMessage = require('../constants/ErrorMessage')
const ErrorCode = require('../constants/ErrorCode')
const StatusCodes = require('http-status-codes')

function clientError(res, code, message) {
    return res.status(StatusCodes.BAD_REQUEST).json(new ErrorMessage(code, message).response())
}


function clientException(res, message) {
    return res.status(StatusCodes.BAD_REQUEST).json(new ErrorMessage(ErrorCode.EXCEPTION, message || 'exception').response())
}

module.exports = {
    clientError,
    clientException
}