const express = require('express');
const bodyParser = require('body-parser'); // convert the body of incoming requests into JavaScript objects.
const cors = require('cors'); //  add headers stating that your API accepts requests coming from other origins
const helmet = require('helmet'); // helps to secure Express APIs by defining various HTTP headers.
const morgan = require('morgan'); // adds some logging capabilities to your Express API.

const path = require("path");
const app = express();
const port = process.env.PORT || "8000";

app.use(express.static(path.join(__dirname, "public")));// serving static files

// adding Helmet to enhance your API's security
app.use(helmet());
// using bodyParser to parse JSON bodies into JS objects
app.use(bodyParser.json());
// enabling CORS for all requests
app.use(cors());
// adding morgan to log HTTP requests
app.use(morgan('combined'));

app.listen(port, () => {
  console.log(`Listening to requests on http://localhost:${port}`);
});

module.exports.app = app;

// config routes
routes = require('./routes')